package nl.wur.ssb.RDFSimpleCon;

import com.github.jsonldjava.core.RDFDataset;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.vfs2.FileObject;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RiotException;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.update.UpdateAction;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdtjena.HDTGraph;

public class RDFSimpleCon
{
  private Model localDb;
  private Dataset dataset;
  public String server;
  public File directory;
  private int port;
  private String finalLocation;
  private String graph;
  private HttpClient authen;
  private boolean eachThreadSeperate = false;
  private int counter = 0;
  private HashMap<Long, Integer> threadMap = new HashMap<Long, Integer>();
  private int maxThreadCount = 1;

  public RDFSimpleCon(String config, String tmpDir) throws Exception
  {
    try
    {
      if (config.indexOf("[") != -1)
      {
        Matcher temp = Pattern.compile("(.*)\\[(.*)\\]").matcher(config);
        if (temp.matches() == false)
          throw new Exception("invalid config string: " + config);
        config = temp.group(1);
        graph = temp.group(2);
      }
      if (config.isEmpty())
      {
        dataset = createEmptyStore(tmpDir);
        if (graph == null)
          localDb = dataset.getDefaultModel();
        else
          localDb = dataset.getNamedModel(graph);
      }
      else if (config.startsWith("file://"))
      {
        String fileName = config.substring("file://".length());
        RDFFormat fileFormat = null;
        if (config.indexOf("{") != -1)
        {
          Matcher temp = Pattern.compile("(.*)\\{(.*)\\}").matcher(fileName);
          if (temp.matches() == false)
            throw new Exception("invalid config string: " + config);
          fileName = temp.group(1);
          fileFormat = RDFFormat.getFormat(temp.group(2));
        }
        File file = new File(fileName);

        if (file.isDirectory())
        {
          // To be able to obtain the dir after creation
          directory = file;
          dataset = TDBFactory.createDataset(file.toString());
        }
        else
        {
          dataset = createEmptyStore(tmpDir);
          RDFDataMgr.read(dataset,file.toString(),fileFormat != null ? fileFormat.getLang() : null);
        }

        if (graph == null)
          localDb = dataset.getDefaultModel();
        else
          localDb = dataset.getNamedModel(graph);
      }
      else
      {
        String username = null;
        String pass = null;
        if (config.indexOf("@") != -1)
        {
          String temp[] = config.split("@");
          String temp2[] = temp[0].split(":");
          if (temp2.length != 2)
            throw new Exception("invalid config string: " + config);
          username = temp2[0];
          pass = temp2[1];
          config = temp[1];
        }
        
        Matcher matcher = Pattern.compile("http://(.+):([\\d]+)/(.*)").matcher(config);
        if (!matcher.matches())
        {
          matcher = Pattern.compile("http://(.+)/(.*)").matcher(config);
          matcher.matches();
          this.server = matcher.group(1);
          this.port = 80;
          this.finalLocation = matcher.group(2);
        }
        else
        {
          this.server = matcher.group(1);
          this.port = Integer.parseInt(matcher.group(2));
          this.finalLocation = matcher.group(3);
        }
        if (username != null)
          this.setAuthen(username,pass);
        // only used for prefixes
        dataset = createEmptyStore(tmpDir);
        localDb = dataset.getDefaultModel();
      }
    }
    catch (Throwable th)
    {
      throw new Exception("invalid config string: " + config,th);
    }
    this.setNsPrefix("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    this.setNsPrefix("rdfs","http://www.w3.org/2000/01/rdf-schema#");
    this.setNsPrefix("owl","http://www.w3.org/2002/07/owl#");
  }
  
  private Dataset createEmptyStore(String tmpDir)
  {
    if (tmpDir == null)
      return TDBFactory.createDataset();
    else
      return TDBFactory.createDataset(tmpDir);
  }
  
  public RDFSimpleCon(String config) throws Exception
  {
    this(config,null);
  }
  
  public RDFSimpleCon(FileObject file, RDFFormat format) throws IOException
  {
    Dataset dataset = createEmptyStore(null);
    RDFDataMgr.read(dataset,file.getContent().getInputStream(),format.getLang());
  }
  
  /*
   * public RDFConnection(String dir,String graph,boolean local) { Dataset
   * dataset = TDBFactory.createDataset(dir); localDb =
   * dataset.getNamedModel(graph); }
   * 
   * public RDFConnection(String server,String graph) {
   * this.setServerGraph(server,graph); }
   */
  
  public void enableEachThreadSeperatePort(int threadCount)
  {
    this.eachThreadSeperate = true;
    this.maxThreadCount = threadCount;
  }
  
  public void setAuthen(String user, String pass)
  {
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
    Credentials credentials = new UsernamePasswordCredentials(user, pass);
    credsProvider.setCredentials(AuthScope.ANY, credentials);
    authen = HttpClients.custom()
        .setDefaultCredentialsProvider(credsProvider)
        .build();
  }
  
  private QueryExecution createQueryFromFile(String queryFile, Object... args) throws Exception
  {
    Object toPass[] = args;
    
    String header = Util.readFile("queries/header.txt");
    String content = Util.readFile(queryFile);
    String queryString = header + content;
    Pattern path = Pattern.compile("^((FRoM)|(WITH)|(USING))\\s+<\\%\\d+\\$S>.*$",Pattern.MULTILINE | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    if (path.matcher(queryString).find())
    {
      toPass = new Object[args.length + 1];
      System.arraycopy(args,0,toPass,1,args.length);
      toPass[0] = this.graph;
      args = toPass;
      if (this.graph == null)
      {
        queryString = path.matcher(queryString).replaceAll("");
      }
    }
    queryString = String.format(queryString,args);
    // System.out.println(queryString);
    return createQuery(queryString);
  }
  
  private QueryExecution createQuery(String queryString) throws Exception
  {
    Query query = QueryFactory.create(queryString);
    if (this.server != null)
    {
      int port = this.port;
      if (this.eachThreadSeperate)
      {
        port = this.getThreadPortNum();
      }
      String server = "http://" + this.server + ":" + port + "/" + this.finalLocation;
      QueryExecution qe = QueryExecutionFactory.sparqlService(server,query,authen);
      qe.setTimeout(7,TimeUnit.DAYS);
      return qe;
    }
    else
    {
      return QueryExecutionFactory.create(query,this.localDb,null);
    }
  }
  
  private int getThreadPortNum() throws Exception
  {
    long threadId = Thread.currentThread().getId();
    if (this.threadMap.containsKey(threadId))
      return this.threadMap.get(threadId) + this.port;
    int newCount = this.counter++;
    if (newCount > this.maxThreadCount)
      throw new Exception("max thread count reached");
    this.threadMap.put(threadId,newCount);
    return newCount + this.port;
  }
  
  public LinkedList<ResultLine> runQueryToMap(String queryFile, Object... args) throws Exception
  {
    queryFile = "queries/" + queryFile;
    QueryExecution qe = createQueryFromFile(queryFile,args);
    long millis = System.currentTimeMillis();
    ResultSet result = qe.execSelect();
    Iterable<HashMap<String, RDFNode>> walker = new Iteration<HashMap<String, RDFNode>>(new ResultIteratorRaw(result));
    LinkedList<ResultLine> res = new LinkedList<ResultLine>();
    for (HashMap<String, RDFNode> item : walker)
    {
      res.add(new ResultLine(item));
    }
    qe.close();
    // System.out.println("time: " + (System.currentTimeMillis() - millis) + "
    // for query " +
    // queryFile);
    return res;
  }
  
  public Iterable<ResultLine> runQuery(String queryFile, boolean preload, Object... args) throws Exception
  {
    queryFile = "queries/" + queryFile;
    QueryExecution qe = createQueryFromFile(queryFile,args);
    // System.out.println(qe.getQuery());
    long millis = System.currentTimeMillis();
    ResultSet result = null;
    try
    {
      result = qe.execSelect();
    }
    catch (QueryExceptionHTTP e)
    {
      System.out.println(e.getResponseMessage());
      throw e;
    }
    ResultIteratorRaw walker = new ResultIteratorRaw(result);// new
                                                             // Iteration<HashMap<String,RDFNode>>
    if (preload == false)
    {
      return new Iteration<ResultLine>(new ResultIterator(new Iteration<HashMap<String, RDFNode>>(walker),qe));
    }
    else
    {
      LinkedList<HashMap<String, RDFNode>> res = new LinkedList<HashMap<String, RDFNode>>();
      for (HashMap<String, RDFNode> item : new Iteration<HashMap<String, RDFNode>>(walker))
      {
        res.add(item);
      }
      qe.close();
      // System.out.println("time: " + (System.currentTimeMillis() - millis) + "
      // for query " +
      // queryFile);
      return new Iteration<ResultLine>(new ResultIterator(new Iteration<HashMap<String, RDFNode>>(res.iterator()),null));
    }
  }
  
  public ResultLine runQuerySingleRes(String queryFile,boolean allowNull,Object... args) throws Exception
  {
    Iterator<ResultLine> itt = this.runQuery(queryFile,true,args).iterator();
    if(!itt.hasNext())
    {
      if(allowNull)
        return null;
      else
        throw new Exception("expect at least one result");
    }
    ResultLine toRet = itt.next();
    if(itt.hasNext())
      throw new Exception("expect no more then one result");
    return toRet;      
  }
  
  public ResultSet runQueryDirect(String query) throws Exception
  {
    QueryExecution qe = createQuery(query);
    return qe.execSelect();
  }
  
  public String expand(String in)
  {
    String toRet = localDb.expandPrefix(in);
    if (!toRet.startsWith("http"))
      throw new RuntimeException("prefix not expanded: " + in);
    return toRet;
  }
  
  public Resource createResource(String iri)
  {
    return this.localDb.createResource(expand(iri));
  }
  
  public void add(String subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createResource(expand(obj)));
    }
  }
  
  public void add(Resource subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createResource(expand(obj)));
    }
  }
  
  public void add(Resource subj, String pred, Resource obj)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),obj);
    }
  }
  
  public void add(String subj, String pred, Resource obj)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),obj);
    }
  }
  
  public void rem(String subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createResource(expand(obj)));
    }
  }
  
  public void rem(Resource subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createResource(expand(obj)));
    }
  }
  
  public void rem(String subj, String pred, RDFNode obj)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),obj);
    }
  }
  
  public void rem(Resource subj, String pred, RDFNode obj)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),obj);
    }
  }
  
  public void addLit(String subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(obj));
    }
  }
  
  public void addLit(Resource subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(obj));
    }
  }
  
  public void remLit(String subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(obj));
    }
  }
  
  public void remLit(Resource subj, String pred, String obj)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(obj));
    }
  }
  
  public void add(String subj, String pred, int val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(String subj, String pred, long val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }  
  
  public void add(Resource subj, String pred, int val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(String subj, String pred, int val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(Resource subj, String pred, int val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(Resource subj, String pred, long val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(Resource subj, String pred, boolean val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(String subj, String pred, boolean val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(String subj, String pred, boolean val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(Resource subj, String pred, boolean val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  
  public void add(String subj, String pred, float val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(Resource subj, String pred, float val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(String subj, String pred, float val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(Resource subj, String pred, float val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(String subj, String pred, double val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(Resource subj, String pred, double val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(String subj, String pred, double val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(Resource subj, String pred, double val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void add(String subj, String pred, LocalDate val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdate));
    }
  }
  
  public void add(Resource subj, String pred, LocalDate val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdate));
    }
  }
  
  public void rem(String subj, String pred, LocalDate val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdate));
    }
  }
  
  
  public void rem(Resource subj, String pred, LocalDate val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdate));
    }
  }
  
  public void add(String subj, String pred, LocalDateTime val)
  {
    synchronized (this)
    {
      this.localDb.add(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdateTime));
    }
  }
  
  public void add(Resource subj, String pred, LocalDateTime val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdateTime));
    }
  }
  
  public void rem(String subj, String pred, LocalDateTime val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdateTime));
    }
  }
  
  public void rem(Resource subj, String pred, LocalDateTime val)
  {
    synchronized (this)
    {
      this.localDb.remove(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(val,XSDDatatype.XSDdateTime));
    }
  }
  
  public void add(Resource subj, String pred, long val)
  {
    synchronized (this)
    {
      this.localDb.add(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public void rem(String subj, String pred, long val)
  {
    synchronized (this)
    {
      this.localDb.remove(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(val));
    }
  }
  
  public boolean contains(String subj, String pred)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)));
    }
  }
  
  public boolean contains(Resource subj, String pred)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)));
    }
  }
  
  public boolean contains(String subj, String pred, String obj)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createResource(expand(obj)));
    }
  }
  
  public boolean contains(Resource subj, String pred, String obj)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createResource(expand(obj)));
    }
  }
  
  public boolean contains(Resource subj, String pred, Resource obj)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),
          obj);
    }
  }
  
  public boolean containsLit(String subj, String pred, String lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(lit));
    }
  }
  
  public boolean containsLit(Resource subj, String pred, String lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createLiteral(lit));
    }
  }
  
  public boolean containsLit(String subj, String pred, int lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(String subj, String pred, float lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(String subj, String pred, boolean lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(String subj, String pred, double lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(Resource subj, String pred, int lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(String subj, String pred, long lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(Resource subj, String pred, long lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(Resource subj, String pred, boolean lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
  
  public boolean containsLit(Resource subj, String pred, float lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }
   
  public boolean containsLit(Resource subj, String pred, double lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),this.localDb.createTypedLiteral(lit));
    }
  }  
  
  public boolean contains(String subj, String pred, LocalDate lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(lit,XSDDatatype.XSDdate));
    }
  }
  
  public boolean contains(Resource subj, String pred, LocalDate lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(lit,XSDDatatype.XSDdate));
    }
  }
  
  public boolean contains(String subj, String pred, LocalDateTime lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(lit,XSDDatatype.XSDdateTime));
    }
  }
  
  public boolean contains(Resource subj, String pred, LocalDateTime lit)
  {
    synchronized (this)
    {
      return this.localDb.contains(subj,this.localDb.createProperty(expand(pred)),
          this.localDb.createTypedLiteral(lit,XSDDatatype.XSDdateTime));
    }
  }
  
  public StmtIterator listPatternLit(String subj, String pred, boolean lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(Resource subj, String pred, boolean lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  
  public StmtIterator listPatternLit(String subj, String pred, char lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(Resource subj, String pred, char lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(String subj, String pred, int lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(Resource subj, String pred, int lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
    
  public StmtIterator listPatternLit(String subj, String pred, float lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  public StmtIterator listPatternLit(Resource subj, String pred, float lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(String subj, String pred, double lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(Resource subj, String pred, double lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(String subj, String pred, long lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(this.localDb.createResource(expand(subj)),this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPatternLit(Resource subj, String pred, long lit)
  {
    synchronized (this)
    {
      return this.localDb.listLiteralStatements(subj,this.localDb.createProperty(expand(pred)),lit);
    }
  }
  
  public StmtIterator listPattern(String subj, String pred, RDFNode obj)
  {
    synchronized (this)
    {
      Resource subjR = null;
      if (subj != null)
        subjR = this.localDb.createResource(expand(subj));
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      return this.localDb.listStatements(subjR,predR,obj);
    }
  }
  
  public StmtIterator listPattern(Resource subj, String pred, RDFNode obj)
  {
    synchronized (this)
    {
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      return this.localDb.listStatements(subj,predR,obj);
    }
  }
  
  public StmtIterator listPatternIri(String subj, String pred, String lit)
  {
    synchronized (this)
    {
      Resource subjR = null;
      if (subj != null)
        subjR = this.localDb.createResource(expand(subj));
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      Resource objR = null;
      if (pred != null)
        objR = this.localDb.createProperty(expand(lit));
      return this.localDb.listStatements(subjR,predR,objR);
    }
  }
  
  public StmtIterator listPatternLit(String subj, String pred, String lit)
  {
    synchronized (this)
    {
      Resource subjR = null;
      if (subj != null)
        subjR = this.localDb.createResource(expand(subj));
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      return this.localDb.listStatements(subjR,predR,lit);
    }
  }
  
  public void removeObject(String subj)
  {
    synchronized (this)
    {
      Resource subjR = this.localDb.createResource(expand(subj));
      this.localDb.removeAll(subjR,null,null);
    }
  }
  
  public void removeObject(Resource subj)
  {
    synchronized (this)
    {
      this.localDb.removeAll(subj,null,null);
    }
  }
  
  public NodeIterator getObjects(Resource subj, String pred)
  {
    synchronized (this)
    {
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      return this.localDb.listObjectsOfProperty(subj,predR);
    }
  }
  
  public NodeIterator getObjects(String subj, String pred)
  {
    synchronized (this)
    {
      Resource subjR = null;
      if (subj != null)
        subjR = this.localDb.createResource(expand(subj));
      Property predR = null;
      if (pred != null)
        predR = this.localDb.createProperty(expand(pred));
      return this.localDb.listObjectsOfProperty(subjR,predR);
    }
  }
  
  /*
   * public boolean bgp(String subj,String pred,String obj) {
   * 
   * }
   */
  
  public PrefixMapping setNsPrefix(String prefix, String iri)
  {
    return this.localDb.setNsPrefix(prefix,iri);
  }
  
  public void close()
  {
    this.localDb.close();
    this.dataset.close();
  }
  
  public Model getModel()
  {
    return this.localDb;
  }
  
  public void save(String file) throws IOException
  {
    this.save(file,RDFFormat.TURTLE);
  }
  
  public void save(String file, RDFFormat format) throws IOException
  {
    // "RDF/XML", "RDF/XML-ABBREV", "N-TRIPLE", "TURTLE", (and "TTL") and "N3
    OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
    this.localDb.write(out,format.toString());
    out.close();
  }
  
  public void runUpdateQuery(String file, Object... args)
  {
    try
    {
      String header = Util.readFile("queries/header.txt");
      String content = Util.readFile("queries/" + file);
      String query = header + content;
      query = String.format(query,args);
      UpdateAction.parseExecute(query,this.localDb);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  public void addAll(RDFSimpleCon other)
  {
    this.localDb.add(other.localDb.listStatements());
    this.localDb.setNsPrefixes(other.getModel().getNsPrefixMap());
  }
  
  public void addAllPrefixes(RDFSimpleCon other)
  {
    this.localDb.setNsPrefixes(other.getModel().getNsPrefixMap());
  }
  
  public Map<String,String> getPrefixMap()
  {
    return this.localDb.getNsPrefixMap();
  }
  
  public String getShortForm(String uri)
  {
    String shortForm = this.localDb.getGraph().getPrefixMapping().shortForm(uri);
    return shortForm.contains("/") ? "<" + expand(uri) + ">" : shortForm;
  }
   
  public boolean containsAll(RDFSimpleCon other)
  {
    return this.localDb.containsAll(other.localDb);
  }
  
  public boolean containsAny(RDFSimpleCon other)
  {
    return this.localDb.containsAny(other.localDb);
  }
  
  public Iterable<ResultLine> runQuery(HDT hdtFile, String queryFile, Object... args) throws Exception
  {
    // System.out.println(queryFile);
    
    // HEADER
    // InputStream fullPath = Util.getResourceFile("queries/header.txt");
    // String HEADER = IOUtils.toString(fullPath, "UTF-8");
    // QUERY
    // fullPath = Util.getResourceFile("queries/" + queryFile);
    // String SPARQL = IOUtils.toString(fullPath, "UTF-8");
    
    // TODO Modify using jesse REGEX method
    // System.out.println(
    // "QUERY AS IT SHOULD BE: \n" + createQueryFromFile("queries/" + queryFile,
    // args).getQuery());
    
    String query = createQueryFromFile("queries/" + queryFile,args).getQuery().toString();
    // System.exit(1);
    //
    long millis = System.currentTimeMillis();
    queryFile = "queries/" + queryFile;
    // long millis = System.currentTimeMillis();
    ResultSet result = null;
    HDTGraph graph = new HDTGraph(hdtFile);
    Model model = ModelFactory.createModelForGraph(graph);
    QueryExecution qe = QueryExecutionFactory.create(query,model);
    result = qe.execSelect();
    ResultIteratorRaw walker = new ResultIteratorRaw(result);// new
    // Iteration<HashMap<String,RDFNode>>
    LinkedList<HashMap<String, RDFNode>> res = new LinkedList<HashMap<String, RDFNode>>();
    for (HashMap<String, RDFNode> item : new Iteration<HashMap<String, RDFNode>>(walker))
    {
      res.add(item);
    }
    qe.close();
    // System.out.println("time: " + (System.currentTimeMillis() - millis) + "ms
    // for query " +
    // queryFile);
    return new Iteration<ResultLine>(new ResultIterator(new Iteration<HashMap<String, RDFNode>>(res.iterator()),null));
  }
  /*
   * Based on the code from org.apache.jena.riot.writer.JenaRDF2JSONLD
   */
  public RDFDataset createJSONLDApiGraph()
  {
    RDFDataset result = new RDFDataset();
    
    StmtIterator iter = this.localDb.listStatements();
    for (; iter.hasNext();)
    {
      Statement q = iter.next();
      Resource s = q.getSubject();
      Resource p = q.getPredicate();
      RDFNode o = q.getObject();
      
      String sq = blankNodeOrIRIString(s);
      if (sq == null)
        throw new RiotException("Subject node is not a URI or a blank node");
      
      String pq = p.getURI();
      if (o.isLiteral())
      {
        String lex = o.asLiteral().getLexicalForm();
        String lang = o.asLiteral().getLanguage();
        String dt = o.asLiteral().getDatatypeURI();
        if (lang != null && lang.length() == 0)
          lang = null;
        if (dt == null)
          dt = XSDDatatype.XSDstring.getURI();
        
        result.addQuad(sq,pq,lex,dt,lang,null);
      }
      else
      {
        String oq = blankNodeOrIRIString(o);
        if (oq == null)
          throw new RiotException("Object node is not a URI, a blank node or literal: " + s + " " + p + " " +o);
        result.addQuad(sq,pq,oq,null);
      }
    }
    
    return result;
  }
    
  private String blankNodeOrIRIString(RDFNode x)
  {
      if ( x.isURIResource()) return x.asResource().getURI() ;
      if ( x.isAnon() ) 
        return x.asNode().getBlankNodeLabel();
      return null ;
  }
  
  public void addAllFromJSONLDApiGraph(RDFDataset toAdd)
  {
    List<com.github.jsonldjava.core.RDFDataset.Quad> quads = toAdd.getQuads("@default");
    for(com.github.jsonldjava.core.RDFDataset.Quad quad : quads)
    {
      this.localDb.add(
          (Resource)toJenaNode(quad.getSubject()),
          this.localDb.createProperty(quad.getPredicate().getValue()),
          toJenaNode(quad.getObject()));     
    }
  }
  private RDFNode toJenaNode(com.github.jsonldjava.core.RDFDataset.Node node)
  {
    if(node.isIRI())
      return this.localDb.createResource(node.getValue());
    else if(node.isBlankNode())
      return this.localDb.createResource(AnonId.create(node.getValue()));
    else if(node.isLiteral())
      return this.localDb.createTypedLiteral(node.getValue(),node.getDatatype());
    return null;
  }
}
