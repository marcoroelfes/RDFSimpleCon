package nl.wur.ssb.RDFSimpleCon.api;

import org.apache.jena.rdf.model.Resource;

public interface OWLThing
{
  public void validate();
  public Resource getResource();
  public String getClassTypeIri();
}
