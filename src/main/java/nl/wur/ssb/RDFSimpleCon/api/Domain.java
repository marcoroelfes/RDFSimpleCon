package nl.wur.ssb.RDFSimpleCon.api;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.WeakHashMap;

import org.apache.commons.vfs2.FileObject;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;

public class Domain
{
  RDFSimpleCon con;
  protected boolean check = true;

  //Weak hashmap loses its items if they are no longer used, this cleans up the memory is the objects are no longer used in the program
  private WeakHashMap<Resource,OWLThingImpl> objects = new WeakHashMap<Resource,OWLThingImpl>();
  public Domain(RDFSimpleCon con)
  {
    this.con = con;
  }
  
  public void test2()
  {
    
  }
  
  public static void main(String args[]) throws Exception
  {
    System.out.println(Domain.class.getMethod("make",String.class,String.class).getReturnType());
  }
  
  public Domain(String config, String tmpDir) throws Exception
  {
    this(new RDFSimpleCon(config,tmpDir));
  }
  
  public Domain(String config) throws Exception
  {
    this(config,null);
  }
  
  public Domain(FileObject file, RDFFormat format) throws IOException
  {
    this(new RDFSimpleCon(file,format));
  }
  /*
   * Only accessible from OWLThing
   */
  OWLThingImpl getObject(Resource resource)
  {
    return this.objects.get(resource);  
  }
  
  void setObject(Resource resource,OWLThingImpl object)
  {
    this.objects.put(resource,object);
  }
  
  void delObject(Resource resource)
  {
    this.objects.remove(resource);
  }
 
  public <T extends OWLThing> T make(String clazzName,String iri)
  {
    try
    {
      return this.make(Class.forName(clazzName),iri);
    }
    catch (ClassNotFoundException e)
    {
      throw new RuntimeException("Class not found",e);
    }
  }
   
  public <T extends OWLThing> T make(Class clazz,String iri)
  {
    if(iri != null)
    {
      iri = iri.replaceAll(" +", "_");
      return this.make(clazz,con.createResource(iri),false);
    }
    else
      return this.make(clazz,con.getModel().createResource(),false);
  }
  
  <T extends OWLThing> T make(Class clazz,Resource resource,boolean direct)
  {
    //TODO validate
    String className = clazz.getName();
    int index = className.lastIndexOf(".");
    Class toBuild = null;
    try
    {
      toBuild = Class.forName(className.substring(0,index) + ".impl" + className.substring(index) + "Impl");
    }
    catch (ClassNotFoundException e1)
    { }
    if(toBuild == null)
      throw new RuntimeException("Can only create classes belonging to the ontolgoy");
    try
    {
      return (T)toBuild.getMethod("make",Domain.class,Resource.class,boolean.class).invoke(null,this,resource,direct);
    }
    catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException e)
    {
      throw new RuntimeException("Internal error make function not found",e);
    }
    catch(InvocationTargetException ie)
    {
      throw new RuntimeException(ie.getCause().getMessage(),ie);
    }
  }
  
  void checkType(Class type,Class expectedType)
  { 
    if(!this.checkTypeRec(type,expectedType))
      throw new RuntimeException("Object is not of expected type: " + type + " - expected " + expectedType);
  }
  
  /*
   * Currently we only allow one type per object, it should possible to modify stuff such multiple type can theoretically supported
   */
  private Class getType(Resource resource,boolean check)
  {
    NodeIterator it = this.con.getObjects(resource,"rdf:type");
    if(it.hasNext())
    {
      RDFNode toRet = it.next();
      if(it.hasNext())
        throw new RuntimeException("Multiple types on single instance are not allowed");
      if(!toRet.isResource())
        throw new RuntimeException("Type def should be resource");
      //TODO later we can further upgrade this
      String type = toRet.asResource().getURI();
      Class clazz = null;
      String basePackage = OWLThingImpl.getBasePackage(type);
      try
      {
        clazz = Class.forName(basePackage + ".domain." + type.replaceAll("^.*[/#]",""));
      }
      catch (ClassNotFoundException e) { }
      if(clazz == null)
      {
        try
        {
          clazz = Class.forName(basePackage + ".template." + type.replaceAll("^.*[/#]",""));
        }
        catch (ClassNotFoundException e)  { }
      }
      if(clazz == null)
        throw new RuntimeException("Defined type: " + type + " not found");
      return clazz;
    }
    if(check)
      throw new RuntimeException("Object is external ref or has no type defined");
    return null;
  }
  
  public OWLThingImpl getObjectFromResource(Resource resource,Class expectedType,boolean check)
  {
    Class type = getType(resource,check);
    if(!check && type == null)
      return null;
    checkType(type,expectedType);
    return make(type,resource,!check);
  }
  
  private boolean checkTypeRec(Class type,Class expectedType)
  { 
    if(type.equals(expectedType))
      return true;
    for(Type parentType : type.getGenericInterfaces())
    {
      if(parentType instanceof Class)
      {
        if(this.checkTypeRec((Class)parentType,expectedType))
          return true;
      }
    }  
    return false;
  }
  
  public OWLThingImpl getObject(String iri,Class clazz)
  {
    return this.getObject(this.con.createResource(iri));
  }
  
  public OWLThingImpl getObject(Resource resource,Class clazz)
  {
    OWLThingImpl toRet = this.getObject(resource);
    if(toRet != null)
    {
      Class meType = (Class)toRet.getClass().getGenericInterfaces()[0];
      if(!checkTypeRec(meType,clazz))
        throw new RuntimeException("Object for iri: " + resource + " already known with other class type: " + meType.getName() + " - expected: " + clazz.getName());
    }
    return toRet;
  }
  
  public void close()
  {
//    this.con.getModel().close();
//    this.getRDFSimpleCon().getModel().close();
    this.con.close();
  }
 
  public void save(String file) throws Exception
  {
    this.con.save(file);
  }
  
  public void save(String file, RDFFormat format) throws IOException
  {
    this.con.save(file,format);
  }

  public RDFSimpleCon getRDFSimpleCon()
  {
    return con;
  }

  public void enableCheck() {
    this.check = true;
  }
  public void disableCheck() {
    this.check = false;
  }
}
