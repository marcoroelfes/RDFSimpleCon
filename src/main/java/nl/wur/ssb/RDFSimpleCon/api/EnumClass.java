package nl.wur.ssb.RDFSimpleCon.api;

public interface EnumClass
{
  public EnumClass[] getParents();
  public String getIRI();
}
