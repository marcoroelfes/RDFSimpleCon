package nl.wur.ssb.RDFSimpleCon.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

public class GetBasePackageTest  
{
  @Test
  public void testBasePackage()
  {
    
    System.out.println(OWLThingImpl.getBasePackage("http://rdfs.org/ns/void#"));
    System.out.println(OWLThingImpl.getBasePackage("http://gbol.life/0.1/"));
    //System.out.println(OWLThingImpl.getBasePackage("http://ssb.wur.nl/RDF2Graph/Error"));
    assertEquals(OWLThingImpl.getBasePackage("http://ssb.wur.nl/RDF2Graph/Error"),"nl.wur.ssb.RDF2Graph");
   // System.out.println(OWLThingImpl.getBasePackage("http://www.w3.org/1999/02/22-rdf-syntax-ns#"));
    assertEquals(OWLThingImpl.getBasePackage("http://www.w3.org/1999/02/22-rdf-syntax-ns#"),"org.w3.rdfsyntaxns");
    System.out.println(OWLThingImpl.getBasePackage("http://www.w3.org/ns/prov#"));
    assertEquals(OWLThingImpl.getBasePackage("http://www.w3.org/ns/prov#"),"org.w3.ns.prov");
   
  }
}
