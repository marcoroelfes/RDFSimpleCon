package nl.wur.ssb.RDFSimpleCon.test;

import static org.junit.Assert.assertEquals;

import com.sun.management.UnixOperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.ext.com.google.common.io.Files;
import org.junit.Test;

public class OpenFileTests
{

  private static OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();

  @Test
  public void testOpenFiles() throws Exception {
    openFiles("Start");
    Domain domain = new Domain("file://" + Files.createTempDir());
    openFiles("Between");
    domain.close();
    openFiles("Close 1");
    domain = new Domain("file://" + Files.createTempDir());
    domain.close();
    openFiles("Close 2");
    domain = new Domain("file://" + Files.createTempDir());
    domain.close();
    openFiles("Close 3");
    domain = new Domain("file://" + Files.createTempDir());
    domain.close();
    openFiles("End");
  }

  public static void openFiles(String key) {
    OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
    System.err.println(key + " <Number of open fd: " + ((UnixOperatingSystemMXBean) os).getOpenFileDescriptorCount());
  }
}
